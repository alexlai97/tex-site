# Course notes

## CS360
- [notes](./TeX_notes/CS360/cs360_self_notes_full.pdf)

## JAPAN202
- [notes](./TeX_notes/JAPAN202R/self_notes/JAPAN202_self_notes.pdf)
- [self made mega quiz](./TeX_notes/JAPAN202R/mega_final_quiz/mega_quiz.pdf)
