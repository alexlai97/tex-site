\contentsline {chapter}{\numberline {1}Lecture 1 九月十日 月曜日}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Vocabulary of Lesson 18: 第十八課の単語}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}自動詞 $\to $ 他動詞}{9}{section.1.2}
\contentsline {section}{\numberline {1.3}transitive v.s. intransitve}{12}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}見える　と　見られる}{12}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}自動詞・他動詞の歌}{12}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Practice on P150 I.A}{12}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Neutral description が...ます}{13}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Practice P151 I.C}{13}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Another Example}{14}{subsection.1.4.2}
\contentsline {chapter}{\numberline {2}Lecture 2 九月十二日 水曜日}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}でしょう/ちゃう}{15}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Practice P153 II.B}{15}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Practice P154 II.C}{16}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Kanji of Lesson 13: 第十四課の漢字}{16}{section.2.2}
\contentsline {chapter}{\numberline {3}Lecture 3 九月十七日 月曜日}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Vocabulary of different fields: 色々分野の単語}{17}{section.3.1}
\contentsline {section}{\numberline {3.2}〜と}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Practice P155 III.A}{17}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}ながら}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Practice P157 IV.A}{18}{subsection.3.3.1}
\contentsline {chapter}{\numberline {4}Lecture 4 九月十九日 水曜日}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}「猫」の慣用語}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}\nobreakspace {}ばよかったです}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Practice P158 V.B}{19}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}第十八課の表現ノート}{20}{section.4.3}
\contentsline {section}{\numberline {4.4}Dialogue of Lesson 18: 第十八課の会話}{20}{section.4.4}
\contentsline {chapter}{\numberline {5}Lecture 5 九月二十四日 月曜日}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}Vocabulary of Lesson 19: 第十九課の単語}{23}{section.5.1}
\contentsline {section}{\numberline {5.2}Honorific Verbs}{25}{section.5.2}
\contentsline {section}{\numberline {5.3}第十九課の表現ノート}{26}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Honorific forms of nouns and adjectives}{26}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}たら in polite speech}{26}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}それで/そして/それから}{26}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Practice P174 I.A.}{27}{section.5.4}
\contentsline {section}{\numberline {5.5}Kanji of Lesson 14: 第十四課の漢字}{28}{section.5.5}
\contentsline {chapter}{\numberline {6}Lecture 6 九月二十六日 水曜日}{29}{chapter.6}
\contentsline {section}{\numberline {6.1}Grammar Exercises I(Lesson 18) 文法練習の宿題の間違いを直す}{29}{section.6.1}
\contentsline {section}{\numberline {6.2}Giving Respectful Advice}{29}{section.6.2}
\contentsline {section}{\numberline {6.3}Practice P176 II.}{30}{section.6.3}
\contentsline {section}{\numberline {6.4}Practice P177 III.A.}{30}{section.6.4}
\contentsline {section}{\numberline {6.5}〜てくれてありがとう}{31}{section.6.5}
\contentsline {section}{\numberline {6.6}Practice P177 III.B.}{31}{section.6.6}
\contentsline {chapter}{\numberline {7}Lecture 7 十月一日 月曜日}{33}{chapter.7}
\contentsline {section}{\numberline {7.1}Practice P178 IV.A.}{33}{section.7.1}
\contentsline {section}{\numberline {7.2}〜はずです}{33}{section.7.2}
\contentsline {chapter}{\numberline {8}Lecture 8 十月三日 水曜日}{35}{chapter.8}
\contentsline {section}{\numberline {8.1}Grammar Exercises II(Lesson 19) 文法練習の宿題の間違いを直す}{35}{section.8.1}
\contentsline {section}{\numberline {8.2}Dialogue of Lesson 19: 第十九課の会話}{35}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}exchange the position of Takeshi and department manager}{36}{subsection.8.2.1}
\contentsline {chapter}{\numberline {9}Lecture 9 十月十二日 金曜日}{37}{chapter.9}
\contentsline {section}{\numberline {9.1}Kanji of Lesson 19: 第十九課の漢字}{37}{section.9.1}
\contentsline {section}{\numberline {9.2}Extra-modest Expressions}{37}{section.9.2}
\contentsline {chapter}{\numberline {10}Lecture 10 十月十五日 月曜日}{39}{chapter.10}
\contentsline {section}{\numberline {10.1}Vocabulary of Lesson 20: 第二十課の単語}{39}{section.10.1}
\contentsline {section}{\numberline {10.2}Continue on Extra-modest Expressions}{41}{section.10.2}
\contentsline {section}{\numberline {10.3}Practice P195 I.A}{41}{section.10.3}
\contentsline {section}{\numberline {10.4}Practice P195 I.C}{41}{section.10.4}
\contentsline {section}{\numberline {10.5}Practice P197 II.A}{41}{section.10.5}
\contentsline {section}{\numberline {10.6}Practice P197 II.B}{41}{section.10.6}
\contentsline {section}{\numberline {10.7}Kanji of Lesson 16: 第十六課の漢字}{41}{section.10.7}
\contentsline {chapter}{\numberline {11}Lecture 11 十月十七日 水曜日}{43}{chapter.11}
\contentsline {section}{\numberline {11.1}Quiz 1の間違いを直す}{43}{section.11.1}
\contentsline {section}{\numberline {11.2}〜ないで}{43}{section.11.2}
\contentsline {section}{\numberline {11.3}Practice P199 III.A}{43}{section.11.3}
\contentsline {section}{\numberline {11.4}Questions within Larger Sentences}{43}{section.11.4}
\contentsline {subsection}{\numberline {11.4.1}Yes/No}{43}{subsection.11.4.1}
\contentsline {subsection}{\numberline {11.4.2}Question-word question}{43}{subsection.11.4.2}
\contentsline {section}{\numberline {11.5}Practice P201 B}{43}{section.11.5}
\contentsline {section}{\numberline {11.6}name という item}{43}{section.11.6}
\contentsline {section}{\numberline {11.7}Practice P202 A}{43}{section.11.7}
\contentsline {chapter}{\numberline {12}Lecture 12 十月二十二日 月曜日}{45}{chapter.12}
\contentsline {section}{\numberline {12.1}〮やすい/〜にくい}{45}{section.12.1}
\contentsline {section}{\numberline {12.2}Practice P203 VII.A}{45}{section.12.2}
\contentsline {section}{\numberline {12.3}Dialogue of Lesson 20: 第二十課の会話}{45}{section.12.3}
\contentsline {section}{\numberline {12.4}Practice P205 A}{46}{section.12.4}
\contentsline {chapter}{\numberline {13}Lecture 13 十月二十四日 水曜日}{47}{chapter.13}
\contentsline {section}{\numberline {13.1}Vocabulary of Lesson 21: 第二十一課の単語}{47}{section.13.1}
\contentsline {section}{\numberline {13.2}Passive Sentences}{49}{section.13.2}
\contentsline {section}{\numberline {13.3}Practice P219 I.A}{49}{section.13.3}
\contentsline {section}{\numberline {13.4}Practice P219 I.B}{49}{section.13.4}
\contentsline {section}{\numberline {13.5}Practice P220 I.D}{49}{section.13.5}
\contentsline {chapter}{\numberline {14}Lecture 14 十月二十九日 月曜日}{51}{chapter.14}
\contentsline {section}{\numberline {14.1}Grammar Exercises III(Lesson 20) 文法練習の宿題の間違いを直す}{51}{section.14.1}
\contentsline {section}{\numberline {14.2}Kanji quiz correction}{51}{section.14.2}
\contentsline {section}{\numberline {14.3}〜てある}{51}{section.14.3}
\contentsline {section}{\numberline {14.4}Practice P221 II.A}{52}{section.14.4}
\contentsline {section}{\numberline {14.5}〜間に}{52}{section.14.5}
\contentsline {section}{\numberline {14.6}Practice P223 III.A}{52}{section.14.6}
\contentsline {chapter}{\numberline {15}Lecture 15 十月三十一日 水曜日}{53}{chapter.15}
\contentsline {section}{\numberline {15.1}Essay Iの間違いを直す}{53}{section.15.1}
\contentsline {section}{\numberline {15.2}Adjective + する}{54}{section.15.2}
\contentsline {section}{\numberline {15.3}Practice P223 IV.A}{54}{section.15.3}
\contentsline {section}{\numberline {15.4}〜てほしい}{54}{section.15.4}
\contentsline {section}{\numberline {15.5}Practice P224 V.B}{54}{section.15.5}
\contentsline {section}{\numberline {15.6}第二十一課の表現ノート}{54}{section.15.6}
\contentsline {section}{\numberline {15.7}Dialogue of Lesson 21: 第二十一課の会話}{54}{section.15.7}
\contentsline {chapter}{\numberline {16}Lecture 16 十一月五日 月曜日}{57}{chapter.16}
\contentsline {section}{\numberline {16.1}Grammar Exercises IVI(Lesson 21) 文法練習の宿題の間違いを直す}{57}{section.16.1}
\contentsline {section}{\numberline {16.2}Oral test 猫の皿}{57}{section.16.2}
\contentsline {section}{\numberline {16.3}Practice P227 VII.B}{57}{section.16.3}
\contentsline {section}{\numberline {16.4}Causative Sentences}{57}{section.16.4}
\contentsline {section}{\numberline {16.5}Practice P240 B.a}{57}{section.16.5}
\contentsline {chapter}{\numberline {17}Lecture 17 十一月十二日 月曜日}{59}{chapter.17}
\contentsline {section}{\numberline {17.1}Vocabulary of Lesson 22: 第二十二課の単語}{59}{section.17.1}
\contentsline {section}{\numberline {17.2}Practice P241 B.b}{61}{section.17.2}
\contentsline {section}{\numberline {17.3}verb stem + なさい}{61}{section.17.3}
\contentsline {section}{\numberline {17.4}Pratice P245 III.A.}{61}{section.17.4}
\contentsline {section}{\numberline {17.5}〜ば}{61}{section.17.5}
\contentsline {section}{\numberline {17.6}Practice P245 IV.A}{61}{section.17.6}
\contentsline {chapter}{\numberline {18}Lecture 18 十一月十四日 水曜日}{63}{chapter.18}
\contentsline {section}{\numberline {18.1}Midterm 中間試験の間違いを直す}{63}{section.18.1}
\contentsline {section}{\numberline {18.2}Practice P247 V.A.}{63}{section.18.2}
\contentsline {section}{\numberline {18.3}Dialogue of Lesson 22: 第二十二課の会話}{63}{section.18.3}
\contentsline {section}{\numberline {18.4}Practice dialogue -- see slides}{64}{section.18.4}
\contentsline {chapter}{\numberline {19}Lecture 19 十一月十九日 月曜日}{65}{chapter.19}
\contentsline {section}{\numberline {19.1}Vocabulary of Lesson 23: 第二十三課の単語}{65}{section.19.1}
\contentsline {section}{\numberline {19.2}Causative-passive sentences}{66}{section.19.2}
\contentsline {section}{\numberline {19.3}Practice P262 I.A.}{66}{section.19.3}
\contentsline {section}{\numberline {19.4}Practice P262 I.B.}{66}{section.19.4}
\contentsline {section}{\numberline {19.5}Practice P262 I.C.}{66}{section.19.5}
\contentsline {chapter}{\numberline {20}Lecture 20 十一月二十六日 月曜日}{67}{chapter.20}
\contentsline {section}{\numberline {20.1}P338 単語}{67}{section.20.1}
\contentsline {section}{\numberline {20.2}作文 II:迷信(superstition) correction}{67}{section.20.2}
\contentsline {section}{\numberline {20.3}P339-341}{68}{section.20.3}
\contentsline {section}{\numberline {20.4}〜ても}{68}{section.20.4}
\contentsline {section}{\numberline {20.5}Practice P264 II.A.}{68}{section.20.5}
\contentsline {section}{\numberline {20.6}Practice P265 D.}{68}{section.20.6}
\contentsline {section}{\numberline {20.7}〜ことにする}{68}{section.20.7}
\contentsline {section}{\numberline {20.8}Practice P266 III.A.}{68}{section.20.8}
\contentsline {chapter}{\numberline {21}Lecture 21 十一月二十八日 水曜日}{69}{chapter.21}
\contentsline {section}{\numberline {21.1}〜まで}{69}{section.21.1}
\contentsline {section}{\numberline {21.2}Practice P268 V.A.}{69}{section.21.2}
\contentsline {section}{\numberline {21.3}〜方}{69}{section.21.3}
\contentsline {section}{\numberline {21.4}Practice P269 VI.A.}{69}{section.21.4}
\contentsline {section}{\numberline {21.5}Dialogue of Lesson 23: 第二十三課の会話}{69}{section.21.5}
\contentsline {chapter}{Bibliography}{71}{chapter*.2}
\contentsfinish 
