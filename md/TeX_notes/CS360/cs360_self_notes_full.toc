\contentsline {chapter}{\numberline {1}Languages}{7}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Language}{7}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Classes of languages}{7}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Language operations}{7}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Proofs about languages}{8}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Deterministic Finite Automata (DFA)}{9}{chapter.2}% 
\contentsline {section}{\numberline {2.1}DFA Definitions}{9}{section.2.1}% 
\contentsline {section}{\numberline {2.2}extended transition function}{9}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Language of an DFA}{9}{section.2.3}% 
\contentsline {chapter}{\numberline {3}Nondeterministic Finite Automata (NFA)}{11}{chapter.3}% 
\contentsline {section}{\numberline {3.1}NFA definition}{11}{section.3.1}% 
\contentsline {section}{\numberline {3.2}new form of extended transition function}{11}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Language of an NFA}{11}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Equivalence of power between NFA and DFA}{11}{section.3.4}% 
\contentsline {chapter}{\numberline {4}epsilon-NFAs}{13}{chapter.4}% 
\contentsline {section}{\numberline {4.1}epsilon-NFA Definition}{13}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Definition the epsilon-closure of a state}{13}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Definition the epsilon-transition function}{14}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Language of epsilon-NFA}{14}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Equivalence of power between epsilon-NFA and DFA}{14}{section.4.5}% 
\contentsline {chapter}{\numberline {5}Regular Expressions}{15}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Definition of Regular Expressions}{15}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Definition Regular Language}{15}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Beginning of regular languages}{16}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Finite languages are regular}{16}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Regular expressions v.s. regular languages}{17}{section.5.5}% 
\contentsline {section}{\numberline {5.6}Examples of regular languages}{17}{section.5.6}% 
\contentsline {section}{\numberline {5.7}Basic rules about regular languages}{17}{section.5.7}% 
\contentsline {chapter}{\numberline {6}Kleene's Theorem}{19}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Kleene's Theorem}{19}{section.6.1}% 
\contentsline {chapter}{\numberline {7}Non-regular languages}{21}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Pumping lemma}{21}{section.7.1}% 
\contentsline {subsection}{\numberline {7.1.1}Definition for pumping lemma}{21}{subsection.7.1.1}% 
\contentsline {subsection}{\numberline {7.1.2}Pumping lemma for Regular language}{21}{subsection.7.1.2}% 
\contentsline {subsection}{\numberline {7.1.3}Definition of Non-regular language}{21}{subsection.7.1.3}% 
\contentsline {subsection}{\numberline {7.1.4}Example of proving non-regularity using pumping lemma}{22}{subsection.7.1.4}% 
\contentsline {subsection}{\numberline {7.1.5}Another example of non-regularity: prime number of 0's}{22}{subsection.7.1.5}% 
\contentsline {subsection}{\numberline {7.1.6}Another example of non-regularity: palindromes}{22}{subsection.7.1.6}% 
\contentsline {subsection}{\numberline {7.1.7}One more example of non-regularity}{22}{subsection.7.1.7}% 
\contentsline {subsection}{\numberline {7.1.8}More pumping lemma: pitfalls}{23}{subsection.7.1.8}% 
\contentsline {section}{\numberline {7.2}Closure properties for regular languages}{23}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Closure rules}{23}{subsection.7.2.1}% 
\contentsline {section}{\numberline {7.3}Decision problems for regular languages}{24}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}Algorithmic questions about finite automata}{24}{subsection.7.3.1}% 
\contentsline {subsection}{\numberline {7.3.2}Acceptance, empty language}{25}{subsection.7.3.2}% 
\contentsline {subsection}{\numberline {7.3.3}Is the language of an FA finite?}{25}{subsection.7.3.3}% 
\contentsline {chapter}{\numberline {8}Context-free grammars and languages}{27}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Context-free grammars and languages}{27}{section.8.1}% 
\contentsline {subsection}{\numberline {8.1.1}Definition of a context-free grammar}{27}{subsection.8.1.1}% 
\contentsline {subsection}{\numberline {8.1.2}A typical example}{27}{subsection.8.1.2}% 
\contentsline {subsection}{\numberline {8.1.3}Derivations}{28}{subsection.8.1.3}% 
\contentsline {subsection}{\numberline {8.1.4}Leftmost and rightmost derivations}{28}{subsection.8.1.4}% 
\contentsline {subsection}{\numberline {8.1.5}Definition of a context-free language}{29}{subsection.8.1.5}% 
\contentsline {subsection}{\numberline {8.1.6}Why called context-free languages}{29}{subsection.8.1.6}% 
\contentsline {subsection}{\numberline {8.1.7}An example of CFL}{29}{subsection.8.1.7}% 
\contentsline {subsection}{\numberline {8.1.8}Another example of CFL}{30}{subsection.8.1.8}% 
\contentsline {section}{\numberline {8.2}Parse trees}{30}{section.8.2}% 
\contentsline {subsection}{\numberline {8.2.1}Definition of parse trees}{30}{subsection.8.2.1}% 
\contentsline {subsection}{\numberline {8.2.2}The yield of a parse tree}{30}{subsection.8.2.2}% 
\contentsline {subsection}{\numberline {8.2.3}An example of palindrom parse trees}{31}{subsection.8.2.3}% 
\contentsline {subsection}{\numberline {8.2.4}An example of a more complicated parse tree}{31}{subsection.8.2.4}% 
\contentsline {subsection}{\numberline {8.2.5}The relationship between leftmost derivations and parse trees}{31}{subsection.8.2.5}% 
\contentsline {section}{\numberline {8.3}Ambiguity in context-free languages}{32}{section.8.3}% 
\contentsline {subsection}{\numberline {8.3.1}An example deriving two parse trees}{32}{subsection.8.3.1}% 
\contentsline {subsection}{\numberline {8.3.2}An example: two derivations with same parse tree}{32}{subsection.8.3.2}% 
\contentsline {subsection}{\numberline {8.3.3}Definition of Ambiguity in CFGs}{32}{subsection.8.3.3}% 
\contentsline {subsection}{\numberline {8.3.4}Annoying example of having ambiguity in code}{33}{subsection.8.3.4}% 
\contentsline {subsection}{\numberline {8.3.5}An example of algebraic expressions}{33}{subsection.8.3.5}% 
\contentsline {subsection}{\numberline {8.3.6}An unambigous grammar and its proof}{33}{subsection.8.3.6}% 
\contentsline {chapter}{\numberline {9}Pushdown automata}{35}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Pushdown automata definitions}{35}{section.9.1}% 
\contentsline {subsection}{\numberline {9.1.1}Pushdown automaton}{35}{subsection.9.1.1}% 
\contentsline {subsection}{\numberline {9.1.2}From an $\epsilon $-NFA to a PDA}{35}{subsection.9.1.2}% 
\contentsline {subsection}{\numberline {9.1.3}Formal definitions of a PDA}{35}{subsection.9.1.3}% 
\contentsline {subsection}{\numberline {9.1.4}Drawing PDAs}{36}{subsection.9.1.4}% 
\contentsline {subsection}{\numberline {9.1.5}Instantaneous description of a PDA}{36}{subsection.9.1.5}% 
\contentsline {subsection}{\numberline {9.1.6}Transitions in the PDA}{37}{subsection.9.1.6}% 
\contentsline {subsection}{\numberline {9.1.7}Bad things in PDAs}{38}{subsection.9.1.7}% 
\contentsline {subsection}{\numberline {9.1.8}A valid computation in a PDA}{38}{subsection.9.1.8}% 
\contentsline {section}{\numberline {9.2}Languages of pushdown automata}{39}{section.9.2}% 
\contentsline {subsection}{\numberline {9.2.1}Acceptance of PDA by final state}{39}{subsection.9.2.1}% 
\contentsline {subsection}{\numberline {9.2.2}An example: $L = \{0^i1^i\}$}{39}{subsection.9.2.2}% 
\contentsline {subsection}{\numberline {9.2.3}Another example: a PDA for palindromes}{40}{subsection.9.2.3}% 
\contentsline {subsection}{\numberline {9.2.4}Acceptance by empty stack}{40}{subsection.9.2.4}% 
\contentsline {subsection}{\numberline {9.2.5}Theorem between those two languages}{40}{subsection.9.2.5}% 
\contentsline {section}{\numberline {9.3}The equivalence of pushdown automata and context-free grammars}{41}{section.9.3}% 
\contentsline {subsection}{\numberline {9.3.1}A example of palindromes to PDA}{41}{subsection.9.3.1}% 
\contentsline {subsection}{\numberline {9.3.2}General structure of construction of PDA from CFG}{41}{subsection.9.3.2}% 
\contentsline {subsection}{\numberline {9.3.3}Proof from CFG to PDA}{42}{subsection.9.3.3}% 
\contentsline {subsection}{\numberline {9.3.4}Proof from PDA to CFG}{43}{subsection.9.3.4}% 
\contentsline {section}{\numberline {9.4}Deterministic PDAs}{43}{section.9.4}% 
\contentsline {subsection}{\numberline {9.4.1}Deterministic PDAs}{43}{subsection.9.4.1}% 
\contentsline {subsection}{\numberline {9.4.2}An example of a language accepted by a DPDA}{43}{subsection.9.4.2}% 
\contentsline {subsection}{\numberline {9.4.3}Not all CFLs are accepted by DPDAs}{44}{subsection.9.4.3}% 
\contentsline {subsection}{\numberline {9.4.4}Overall hierachy of languages}{44}{subsection.9.4.4}% 
\contentsline {chapter}{\numberline {10}Properties of context-free languages}{45}{chapter.10}% 
\contentsline {section}{\numberline {10.1}Normal forms for context-free grammars}{45}{section.10.1}% 
\contentsline {subsection}{\numberline {10.1.1}Normal forms}{45}{subsection.10.1.1}% 
\contentsline {subsection}{\numberline {10.1.2}Identifying nullable variables}{45}{subsection.10.1.2}% 
\contentsline {subsection}{\numberline {10.1.3}Steps in constructing a Chomsky Normal Form}{45}{subsection.10.1.3}% 
\contentsline {subsection}{\numberline {10.1.4}Chomsky Normal Form algorithm}{46}{subsection.10.1.4}% 
\contentsline {section}{\numberline {10.2}Pumping lemma for context-free languages}{46}{section.10.2}% 
\contentsline {subsection}{\numberline {10.2.1}CFL pumpin lemma}{46}{subsection.10.2.1}% 
\contentsline {subsection}{\numberline {10.2.2}Not context-free language}{47}{subsection.10.2.2}% 
\contentsline {subsection}{\numberline {10.2.3}An example: $L = \{a^ib^ic^i | i\leq 0\}$}{47}{subsection.10.2.3}% 
\contentsline {subsection}{\numberline {10.2.4}Another example: $L = \{a^i b ^j c ^k | i < j, i < k \}$}{47}{subsection.10.2.4}% 
\contentsline {subsection}{\numberline {10.2.5}Another exmpale: $L = \{ ss | s \in \{a,b\}^*\}$}{48}{subsection.10.2.5}% 
\contentsline {section}{\numberline {10.3}Closure properties for context-free languages}{48}{section.10.3}% 
\contentsline {section}{\numberline {10.4}Decision algorithms for context-free languages}{48}{section.10.4}% 
\contentsline {chapter}{\numberline {11}Turing machines}{49}{chapter.11}% 
\contentsline {section}{\numberline {11.1}Turing machines}{49}{section.11.1}% 
\contentsline {subsection}{\numberline {11.1.1}Turing machines formal definition}{49}{subsection.11.1.1}% 
\contentsline {subsection}{\numberline {11.1.2}launching the TM}{49}{subsection.11.1.2}% 
\contentsline {subsection}{\numberline {11.1.3}Instantaneous descriptions for TM}{50}{subsection.11.1.3}% 
\contentsline {subsection}{\numberline {11.1.4}Transition in TM}{50}{subsection.11.1.4}% 
\contentsline {subsection}{\numberline {11.1.5}Acceptance in the TM}{50}{subsection.11.1.5}% 
\contentsline {subsection}{\numberline {11.1.6}Language of TM}{51}{subsection.11.1.6}% 
\contentsline {subsection}{\numberline {11.1.7}Rejection in the TM}{51}{subsection.11.1.7}% 
\contentsline {subsection}{\numberline {11.1.8}An example: palindromes}{51}{subsection.11.1.8}% 
\contentsline {subsection}{\numberline {11.1.9}Another exmple $L = \{ s!s | s\in \{a,b \}^*\}$}{53}{subsection.11.1.9}% 
\contentsline {section}{\numberline {11.2}Programming Turing machines and computable functions}{53}{section.11.2}% 
\contentsline {subsection}{\numberline {11.2.1}Programming Turing machines}{53}{subsection.11.2.1}% 
\contentsline {subsection}{\numberline {11.2.2}Accepting or deciding}{53}{subsection.11.2.2}% 
\contentsline {subsection}{\numberline {11.2.3}Recursive v.s. recursively enumerable}{54}{subsection.11.2.3}% 
\contentsline {subsection}{\numberline {11.2.4}Computing a function with Turing machines}{54}{subsection.11.2.4}% 
\contentsline {subsection}{\numberline {11.2.5}Computable functions}{55}{subsection.11.2.5}% 
\contentsline {subsection}{\numberline {11.2.6}Characteristic functions}{55}{subsection.11.2.6}% 
\contentsline {subsection}{\numberline {11.2.7}If we can compute $\chi _L$, then $L$ is decidable}{56}{subsection.11.2.7}% 
\contentsline {subsection}{\numberline {11.2.8}Using subroutines}{56}{subsection.11.2.8}% 
\contentsline {subsection}{\numberline {11.2.9}Storage in the state}{58}{subsection.11.2.9}% 
\contentsline {section}{\numberline {11.3}Variations on a Turing machine}{58}{section.11.3}% 
\contentsline {subsection}{\numberline {11.3.1}Variations on Turing machines}{58}{subsection.11.3.1}% 
\contentsline {subsection}{\numberline {11.3.2}Muti-tape Turing machines}{58}{subsection.11.3.2}% 
\contentsline {chapter}{\numberline {12}Undecidability}{61}{chapter.12}% 
\contentsline {section}{\numberline {12.1}An undecidable language}{61}{section.12.1}% 
\contentsline {subsection}{\numberline {12.1.1}Cardinality and so}{61}{subsection.12.1.1}% 
\contentsline {subsection}{\numberline {12.1.2}An undecidable language}{61}{subsection.12.1.2}% 
\contentsline {subsection}{\numberline {12.1.3}Universal Turing machine}{61}{subsection.12.1.3}% 
\contentsline {subsection}{\numberline {12.1.4}Universal language}{62}{subsection.12.1.4}% 
\contentsline {subsection}{\numberline {12.1.5}Construction of universal turing machine}{62}{subsection.12.1.5}% 
\contentsline {subsection}{\numberline {12.1.6}$L_u$ is not decidable.}{62}{subsection.12.1.6}% 
\contentsline {subsection}{\numberline {12.1.7}Reduction}{63}{subsection.12.1.7}% 
\contentsline {subsection}{\numberline {12.1.8}$L_{SA}$ is not decidable}{63}{subsection.12.1.8}% 
\contentsline {subsection}{\numberline {12.1.9}Closure rules for decidable and recursively enumerable languages}{63}{subsection.12.1.9}% 
\contentsline {section}{\numberline {12.2}Reductions}{64}{section.12.2}% 
\contentsline {subsection}{\numberline {12.2.1}Proper definition of reduction}{64}{subsection.12.2.1}% 
\contentsline {subsection}{\numberline {12.2.2}Using a reduction to prove a language is undecidable or not r.e.}{64}{subsection.12.2.2}% 
\contentsline {subsection}{\numberline {12.2.3}Other undecidable problems about Turing machines}{65}{subsection.12.2.3}% 
\contentsline {subsection}{\numberline {12.2.4}A few other terminology notes}{66}{subsection.12.2.4}% 
\contentsline {subsection}{\numberline {12.2.5}Empty language is undecidable}{66}{subsection.12.2.5}% 
\contentsline {subsection}{\numberline {12.2.6}Infinite language is not decidable}{67}{subsection.12.2.6}% 
\contentsline {subsection}{\numberline {12.2.7}Finite language is not decidable}{67}{subsection.12.2.7}% 
\contentsline {subsection}{\numberline {12.2.8}Language of Turing machines with non-regular languages is undecidable}{67}{subsection.12.2.8}% 
\contentsline {subsection}{\numberline {12.2.9}Language of Turing machines with a Regular Language}{68}{subsection.12.2.9}% 
\contentsline {subsection}{\numberline {12.2.10}Rice's Theorem}{68}{subsection.12.2.10}% 
\contentsline {subsection}{\numberline {12.2.11}Problems regarding decidability}{69}{subsection.12.2.11}% 
\contentsline {section}{\numberline {12.3}Undecidable problems in automata theory}{69}{section.12.3}% 
\contentsline {subsection}{\numberline {12.3.1}Post's Correspondence Problem}{69}{subsection.12.3.1}% 
\contentsline {subsection}{\numberline {12.3.2}Two CFG problems are undecidable}{70}{subsection.12.3.2}% 
\contentsfinish 
