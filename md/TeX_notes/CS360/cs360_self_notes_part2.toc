\contentsline {chapter}{\numberline {1}Context-free grammars and languages}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Context-free grammars and languages}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Definition of a context-free grammar}{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}A typical example}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Derivations}{6}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Leftmost and rightmost derivations}{6}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Definition of a context-free language}{7}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.1.6}Why called context-free languages}{7}{subsection.1.1.6}
\contentsline {subsection}{\numberline {1.1.7}An example of CFL}{7}{subsection.1.1.7}
\contentsline {subsection}{\numberline {1.1.8}Another example of CFL}{8}{subsection.1.1.8}
\contentsline {section}{\numberline {1.2}Parse trees}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Definition of parse trees}{8}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}The yield of a parse tree}{8}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}An example of palindrom parse trees}{9}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}An example of a more complicated parse tree}{9}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}The relationship between leftmost derivations and parse trees}{9}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Ambiguity in context-free languages}{10}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}An example deriving two parse trees}{10}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}An example: two derivations with same parse tree}{10}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Definition of Ambiguity in CFGs}{11}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Annoying example of having ambiguity in code}{11}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}An example of algebraic expressions}{11}{subsection.1.3.5}
\contentsline {subsection}{\numberline {1.3.6}An unambigous grammar and its proof}{11}{subsection.1.3.6}
\contentsline {chapter}{\numberline {2}Pushdown automata}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Pushdown automata definitions}{13}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Pushdown automaton}{13}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}From an $\epsilon $-NFA to a PDA}{13}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Formal definitions of a PDA}{13}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Drawing PDAs}{14}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Instantaneous description of a PDA}{14}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Transitions in the PDA}{15}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Bad things in PDAs}{16}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}A valid computation in a PDA}{16}{subsection.2.1.8}
\contentsline {section}{\numberline {2.2}Languages of pushdown automata}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Acceptance of PDA by final state}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}An example: $L = \{0^i1^i\}$}{17}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Another example: a PDA for palindromes}{18}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Acceptance by empty stack}{18}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Theorem between those two languages}{18}{subsection.2.2.5}
\contentsline {section}{\numberline {2.3}The equivalence of pushdown automata and context-free grammars}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}A example of palindromes to PDA}{19}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}General structure of construction of PDA from CFG}{20}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Proof from CFG to PDA}{20}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Proof from PDA to CFG}{21}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Deterministic PDAs}{21}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Deterministic PDAs}{21}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}An example of a language accepted by a DPDA}{21}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Not all CFLs are accepted by DPDAs}{22}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Overall hierachy of languages}{22}{subsection.2.4.4}
\contentsline {chapter}{\numberline {3}Properties of context-free languages}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Normal forms for context-free grammars}{23}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Normal forms}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Identifying nullable variables}{23}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Steps in constructing a Chomsky Normal Form}{23}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Chomsky Normal Form algorithm}{24}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Pumping lemma for context-free languages}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}CFL pumpin lemma}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Not context-free language}{25}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}An example: $L = \{a^ib^ic^i | i\leq 0\}$}{25}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Another example: $L = \{a^i b ^j c ^k | i < j, i < k \}$}{25}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Another exmpale: $L = \{ ss | s \in \{a,b\}^*\}$}{26}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Closure properties for context-free languages}{26}{section.3.3}
\contentsline {section}{\numberline {3.4}Decision algorithms for context-free languages}{26}{section.3.4}
\contentsline {chapter}{\numberline {4}Turing machines}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Turing machines}{27}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Turing machines formal definition}{27}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}launching the TM}{27}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Instantaneous descriptions for TM}{28}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Transition in TM}{28}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Acceptance in the TM}{28}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Language of TM}{29}{subsection.4.1.6}
\contentsline {subsection}{\numberline {4.1.7}Rejection in the TM}{29}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}An example: palindromes}{29}{subsection.4.1.8}
\contentsline {subsection}{\numberline {4.1.9}Another exmple $L = \{ s!s | s\in \{a,b \}^*\}$}{31}{subsection.4.1.9}
\contentsline {section}{\numberline {4.2}Programming Turing machines and computable functions}{31}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Programming Turing machines}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Accepting or deciding}{31}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Recursive v.s. recursively enumerable}{32}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Computing a function with Turing machines}{32}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Computable functions}{33}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Characteristic functions}{33}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}If we can compute $\chi _L$, then $L$ is decidable}{34}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Using subroutines}{34}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Storage in the state}{36}{subsection.4.2.9}
\contentsline {section}{\numberline {4.3}Variations on a Turing machine}{36}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Variations on Turing machines}{36}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Muti-tape Turing machines}{36}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Undecidability}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}An undecidable language}{39}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Cardinality and so}{39}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}An undecidable language}{39}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Universal Turing machine}{39}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Universal language}{40}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Construction of universal turing machine}{40}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}$L_u$ is not decidable.}{40}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Reduction}{41}{subsection.5.1.7}
\contentsline {subsection}{\numberline {5.1.8}$L_{SA}$ is not decidable}{41}{subsection.5.1.8}
\contentsline {subsection}{\numberline {5.1.9}Closure rules for decidable and recursively enumerable languages}{41}{subsection.5.1.9}
\contentsline {section}{\numberline {5.2}Reductions}{42}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Proper definition of reduction}{42}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Using a reduction to prove a language is undecidable or not r.e.}{42}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Other undecidable problems about Turing machines}{43}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}A few other terminology notes}{44}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Empty language is undecidable}{44}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Infinite language is not decidable}{45}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}Finite language is not decidable}{45}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}Language of Turing machines with non-regular languages is undecidable}{45}{subsection.5.2.8}
\contentsline {subsection}{\numberline {5.2.9}Language of Turing machines with a Regular Language}{46}{subsection.5.2.9}
\contentsline {subsection}{\numberline {5.2.10}Rice's Theorem}{46}{subsection.5.2.10}
\contentsline {subsection}{\numberline {5.2.11}Problems regarding decidability}{47}{subsection.5.2.11}
\contentsline {section}{\numberline {5.3}Undecidable problems in automata theory}{47}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Post's Correspondence Problem}{47}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Two CFG problems are undecidable}{48}{subsection.5.3.2}
\contentsfinish 
