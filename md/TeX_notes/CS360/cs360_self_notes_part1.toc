\contentsline {chapter}{\numberline {1}Languages}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Language}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Classes of languages}{3}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Language operations}{3}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Proofs about languages}{4}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Deterministic Finite Automata (DFA)}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}DFA Definitions}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2}extended transition function}{5}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Language of an DFA}{5}{section.2.3}% 
\contentsline {chapter}{\numberline {3}Nondeterministic Finite Automata (NFA)}{7}{chapter.3}% 
\contentsline {section}{\numberline {3.1}NFA definition}{7}{section.3.1}% 
\contentsline {section}{\numberline {3.2}new form of extended transition function}{7}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Language of an NFA}{7}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Equivalence of power between NFA and DFA}{7}{section.3.4}% 
\contentsline {chapter}{\numberline {4}epsilon-NFAs}{9}{chapter.4}% 
\contentsline {section}{\numberline {4.1}epsilon-NFA Definition}{9}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Definition the epsilon-closure of a state}{9}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Definition the epsilon-transition function}{10}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Language of epsilon-NFA}{10}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Equivalence of power between epsilon-NFA and DFA}{10}{section.4.5}% 
\contentsline {chapter}{\numberline {5}Regular Expressions}{11}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Definition of Regular Expressions}{11}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Definition Regular Language}{11}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Beginning of regular languages}{12}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Finite languages are regular}{12}{section.5.4}% 
\contentsline {section}{\numberline {5.5}Regular expressions v.s. regular languages}{13}{section.5.5}% 
\contentsline {section}{\numberline {5.6}Examples of regular languages}{13}{section.5.6}% 
\contentsline {section}{\numberline {5.7}Basic rules about regular languages}{13}{section.5.7}% 
\contentsline {chapter}{\numberline {6}Kleene's Theorem}{15}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Kleene's Theorem}{15}{section.6.1}% 
\contentsline {chapter}{\numberline {7}Non-regular languages}{17}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Pumping lemma}{17}{section.7.1}% 
\contentsline {subsection}{\numberline {7.1.1}Definition for pumping lemma}{17}{subsection.7.1.1}% 
\contentsline {subsection}{\numberline {7.1.2}Pumping lemma for Regular language}{17}{subsection.7.1.2}% 
\contentsline {subsection}{\numberline {7.1.3}Definition of Non-regular language}{17}{subsection.7.1.3}% 
\contentsline {subsection}{\numberline {7.1.4}Example of proving non-regularity using pumping lemma}{18}{subsection.7.1.4}% 
\contentsline {subsection}{\numberline {7.1.5}Another example of non-regularity: prime number of 0's}{18}{subsection.7.1.5}% 
\contentsline {subsection}{\numberline {7.1.6}Another example of non-regularity: palindromes}{18}{subsection.7.1.6}% 
\contentsline {subsection}{\numberline {7.1.7}One more example of non-regularity}{18}{subsection.7.1.7}% 
\contentsline {subsection}{\numberline {7.1.8}More pumping lemma: pitfalls}{19}{subsection.7.1.8}% 
\contentsline {section}{\numberline {7.2}Closure properties for regular languages}{19}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Closure rules}{19}{subsection.7.2.1}% 
\contentsline {section}{\numberline {7.3}Decision problems for regular languages}{20}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}Algorithmic questions about finite automata}{20}{subsection.7.3.1}% 
\contentsline {subsection}{\numberline {7.3.2}Acceptance, empty language}{21}{subsection.7.3.2}% 
\contentsline {subsection}{\numberline {7.3.3}Is the language of an FA finite?}{21}{subsection.7.3.3}% 
\contentsfinish 
